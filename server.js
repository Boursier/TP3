// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var bcrypt = require('bcrypt');
var cookieParser = require('cookie-parser');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({extended: false});

// Log requests
app.all("*", urlencodedParser, function (req, res, next) {
    console.log(req.method + " " + req.url);
    console.dir(req.headers);
    console.log(req.body);
    console.log();
    next();
});

// Serve static files
app.use(express.static('public'));
app.use(cookieParser());
app.set('view engine', 'ejs');


// Fonctions
function isUserExist(req) {
    return new Promise(function (resolve, reject) {
        db.all("SELECT * FROM users WHERE ident=? AND password=?", [req.body.ident, req.body.password], function (errLogin, data) {
            if (errLogin) {
                reject(errLogin);
            }
            resolve(data);
        });
    });
}

function isUserHaveSession(data) {
    return new Promise(function (resolve, reject) {
        var userId = data[0].ident;
        db.all("SELECT * FROM sessions WHERE ident=?", [userId], function (errSession, sessData) {
            if (errSession) {
                reject(errSession);
            }
            resolve(sessData);
        })
    })
}

function createUserSession(token, userId) {
    return new Promise(function (resolve, reject) {
        db.all("INSERT INTO sessions (ident, token) VALUES (?, ?)", [userId, token], function (err, data) {
            if (err) {
                reject(err);
            }
            resolve();
        });
    });
}

function createToken(userId) {
    var string = '' + Date.now() + userId + Math.random();
    return bcrypt.hashSync(string, 10);
}


// Routes
app.get(['/', '/login'], function (req, res, next) {
    var token = req.cookies.token;
    db.all("SELECT * FROM sessions WHERE token=?", [token], function (err, data) {
        if (err) {
            console.log(err);
        }
        if (data.length == 0) { //Si n'est pas loggé > page login
            res.render('login');
        }
        
        else {
            var userId = data[0].ident;
            res.render('home', {userId: userId});
        }
    })
});



app.get("/deconnect", function (req, res, next) {
    var token = req.cookies.token;
    db.all("SELECT * FROM sessions WHERE token=?", [token], function (err, data) {
        if (err) {
            console.log(err);
        } else {
            db.run("DELETE FROM sessions WHERE token=?", [token], function (err) {
                console.log('supp');
                //res.clearCookie("key"); // Trouver un moyen de supprimer les cookie en même temps
                res.render('login');
            });
        }
    })
})

app.post("/login", function (req, res, next) {
    var userId;
    isUserExist(req).then(function (data) { //L'utilisateur existe t-il ?
        if (data.length == 0) {
            res.render('login', {flashMessage: "Echec d'authentification"});
        } else {
            userId = data[0].ident;
            return isUserHaveSession(data);
        }
    }).catch(function (err) {
        console.log(err);
    }).then(function (sessData) {//As t-il déjà une session ?
        var token = createToken(userId);
        res.cookie('token', token);
        if (sessData.length > 0) {//Si oui on route vers la home
            res.render('home', {userId: userId});
        } else {//Sinon on lui crée une session
            return createUserSession(token, userId);
        }
    }).catch(function (err) {
        console.log(err);
    }).then(function () {
        res.render('home', {userId: userId});
    }).catch(function (err) {
        console.log(err);
    })
});

// Startup server
app.listen(port, function () {
    console.log('Le serveur est accessible sur http://localhost:' + port + "/");
    console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
